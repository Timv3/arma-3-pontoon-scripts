["ace_cargoLoaded", {
        private _item = param[0];
        private _veh = param[1];
        if (typeOf _item == "rhs_pontoon_static" && typeOf _veh != "rhs_pontoon_static" ) then {
            _obj1 = "rhs_pontoon_static" createVehicle [0,0,0];
            _n_pontoons = _veh getVariable "n_pontoons";
            _z = -0.75 + (0.75 * _n_pontoons);
            _obj1 attachTo [_veh, [0,1.8,_z]];
            _veh setVariable ["n_pontoons", _n_pontoons + 1, true];
            _pontoons = _veh getVariable "pontoons";
            _pontoons pushBack _obj1;
            _veh setVariable ["pontoons", _pontoons, true];
        };
}] call CBA_fnc_addEventHandler;

["ace_cargoUnloaded", {
        private _item = param[0];
        private _veh = param[1];
        if (typeOf _item == "rhs_pontoon_static" && typeOf _veh != "rhs_pontoon_static" ) then {
            _pontoons = _veh getVariable "pontoons";
            _i = count _pontoons - 1;
            _pontoon = _pontoons select _i;
            _pontoons deleteAt _i;
            deleteVehicle _pontoon;
            _n_pontoons = _veh getVariable "n_pontoons";
            _veh setVariable ["n_pontoons", _n_pontoons - 1, true];
            _veh setVariable ["pontoons", _pontoons, true];
            _dir = vectorDir _veh;
            _v0 = _dir select 0;
            _v1 = _dir select 1;
            _item hideObjectGlobal true;
            _item setPosWorld [ (getPosWorld _veh select 0) - (8 * _v0), (getPosWorld _veh select 1) - (8 * _v1), (getPosWorld _veh select 2) - 2.3];
            _item hideObjectGlobal false;
        };
}] call CBA_fnc_addEventHandler;
