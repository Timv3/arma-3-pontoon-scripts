# Pontoon Scripts

## Introduction

This scripts works in conjunction with the RHS pontoons and ACE cargo framework. It was used in an Arma 3 operation with 3CB.
This is, however, not officially related to any of these parties.

Example: https://www.youtube.com/watch?v=9dgMFQc6Io8

## Setup

In `init.sqf` is merely the initialization of `truck_load.sqf` and `place_pontoon.sqf`.

`truck_load.sqf` is responsible for catching an ACE unload or load.

`place_pontoon.sqf` is responsible for the pontoon placement.

`truck.sqf` should be in the init of every truck that should be compatibly with this system:
```
null = [this] execVM "truck.sqf";
```

### Classnames in scripts
`truck_load.sqf` contains the RHS pontoon classnames:
- `rhs_pontoon_static`
This is set in order to recognise whether a RHS pontoon is loaded and should be changed if the pontoon classname is different.

## Arma 3 Editor

### Source pontoons
Make RHS source pontoons carryable (init):
```
[this, true, [0,1.5,1], 0] call ace_dragging_fnc_setCarryable;

```
The source pontoon should have an ACE cargo size of 1.

### Target pontoons
Place preplaced RHS target pontoons on the location where you want the bridge to be built. Hide these objects in the Arma editor, to make sure the players cannot see them yet. Set the RHS pontoons in the mission to have ACE cargo space, so a pontoon can be loaded into it. When playing the mission itself, the RHS pontoons are preplaced and thus the players will have to start building at that specific location.

The init field of the RHS target pontoon:
```
this hideObjectGlobal true; 

[this, 1] call ace_cargo_fnc_setSize;
[this, 1] call ace_cargo_fnc_setSpace;
```

### MAN Trucks
Allow 3CB MAN Truck to be used:
```
null = [this] execVM "truck.sqf";
```
Change cargo space of trucks so they can fit more or less pontoons.

This can work with other trucks, but the coordinates of where the pontoons are placed might need to be changed.


## Use in-game

Interact with the truck to unload the pontoons.

Interact with the pontoon to carry it. The pontoon can be carried to where it is needed.

Next interact with the pontoon where it is supposed to be built, then load the pontoon into the pontoon. One pontoon will disappear, the destination pontoon will unhide and be ready for use.
