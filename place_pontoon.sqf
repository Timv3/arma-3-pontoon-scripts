["ace_cargoLoaded", {
    private _item = param[0];
    private _veh = param[1];
     if (typeOf _item == "rhs_pontoon_static" && typeOf _veh == "rhs_pontoon_static" ) then {
        _veh = _veh hideObjectGlobal false;
        [_item, _veh, 1] call ace_cargo_fnc_removeCargoItem;
        [_veh, 0] call ace_cargo_fnc_setSpace;
    }
}] call CBA_fnc_addEventHandler;
